## About

This is an Android application, that displays information about "Rick & Morty" characters. You can check-out list of characters and detailed information about each of them.

For the most part, this is a pet project, created to try-out several technologies and approaches, such as: Jetpack Compose, unidirectional data flow (UDF), GraphQL.

No libraries used for UDF, because I wanted to dive deeper into this approach. So several classes were created to implement TEA-like state & events handling.

## Features

- Jetpack Compose used for UI
- TEA-like UDF with Coroutines Flow (TEA stands for "The Elm architecture")
- "Rick & Morty" API with Apollo GraphQL
- Koin used for dependency injection
- Navigation implemented with Fragments and Cicerone
- Dark or light theme of the app depends on system theme
- Insets are handled with Accompanist to implement translucent status and navigation bars
- Gitlab CI used for building, running JVM tests and sending APK to the Telegram bot

## Screenshots

<img src="/readme/list-port-dark-gest.jpg" align="left"
height="725" hspace="10" vspace="10">

<img src="/readme/char-port-light-nav.jpg" align="center"
height="725" hspace="10" vspace="10">

<img src="/readme/list-land-light-nav.jpg" align="left"
height="300" hspace="10" vspace="10">

<img src="/readme/char-land-dark-gest.jpg" align="center"
height="300" hspace="10" vspace="10">
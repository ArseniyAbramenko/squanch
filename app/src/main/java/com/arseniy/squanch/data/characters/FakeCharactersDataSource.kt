package com.arseniy.squanch.data.characters

import com.arseniy.squanch.common.Either
import com.arseniy.squanch.model.Character
import com.arseniy.squanch.model.fakeCharacters
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

const val FAKE_CHARACTERS_ERROR_MESSAGE = "Fake characters data source error"

class SuccessFakeCharactersDataSource : CharactersDataSource {

    override fun getCharacters(): Flow<Either<String, List<Character>>> = flow {
        emit(Either.Right(fakeCharacters))
    }
}

class ErrorFakeCharactersDataSource : CharactersDataSource {

    override fun getCharacters(): Flow<Either<String, List<Character>>> = flow {
        emit(Either.Left(FAKE_CHARACTERS_ERROR_MESSAGE))
    }
}
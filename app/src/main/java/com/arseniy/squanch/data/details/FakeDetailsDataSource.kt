package com.arseniy.squanch.data.details

import com.arseniy.squanch.common.Either
import com.arseniy.squanch.model.CharacterDetails
import com.arseniy.squanch.model.fakeCharacter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

const val FAKE_DETAILS_ERROR_MESSAGE = "Fake details data source error"

class SuccessFakeDetailsDataSource : DetailsDataSource {

    override fun getDetails(id: String): Flow<Either<String, CharacterDetails>> = flow {
        emit(Either.Right(fakeCharacter))
    }
}

class ErrorFakeDetailsDataSource : DetailsDataSource {

    override fun getDetails(id: String): Flow<Either<String, CharacterDetails>> = flow {
        emit(Either.Left(FAKE_DETAILS_ERROR_MESSAGE))
    }
}
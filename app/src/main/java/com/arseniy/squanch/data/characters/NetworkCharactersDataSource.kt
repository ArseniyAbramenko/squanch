package com.arseniy.squanch.data.characters

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.toFlow
import com.arseniy.squanch.CharactersQuery
import com.arseniy.squanch.common.Either
import com.arseniy.squanch.extensions.toEither
import com.arseniy.squanch.model.Character
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class NetworkCharactersDataSource(private val client: ApolloClient) : CharactersDataSource {

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getCharacters(): Flow<Either<String, List<Character>>> =
        client.query(CharactersQuery()).toFlow().map { response ->
            response.toEither {
                data?.characters?.results?.mapNotNull { networkCharacter ->
                    networkCharacter?.let {
                        if (it.id != null) Character(
                            id = it.id,
                            name = it.name ?: "",
                            status = it.status ?: "",
                            species = it.species ?: "",
                            imageUrl = it.image ?: ""
                        )
                        else null
                    }
                }
            }
        }
}
package com.arseniy.squanch.data.details

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.coroutines.toFlow
import com.arseniy.squanch.CharacterQuery
import com.arseniy.squanch.common.Either
import com.arseniy.squanch.extensions.toEither
import com.arseniy.squanch.model.CharacterDetails
import com.arseniy.squanch.model.Location
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class NetworkDetailsDataSource(private val client: ApolloClient) : DetailsDataSource {

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun getDetails(id: String): Flow<Either<String, CharacterDetails>> =
        client.query(CharacterQuery(id)).toFlow().map { response ->
            response.toEither {
                data?.character?.let { character ->
                    if (character.id != null) CharacterDetails(
                        id = character.id,
                        name = character.name ?: "",
                        status = character.status ?: "",
                        species = character.species ?: "",
                        origin = character.origin.toDomain(),
                        location = character.location.toDomain(),
                        imageUrl = character.image ?: ""
                    )
                    else null
                }
            }
        }

    private fun CharacterQuery.Origin?.toDomain(): Location? = this?.let {
        if (it.id != null) Location(
            id = it.id,
            name = it.name ?: ""
        )
        else null
    }

    private fun CharacterQuery.Location?.toDomain(): Location? = this?.let {
        if (it.id != null) Location(
            id = it.id,
            name = it.name ?: ""
        )
        else null
    }
}



package com.arseniy.squanch.data.details

import com.arseniy.squanch.common.Either
import com.arseniy.squanch.model.CharacterDetails
import kotlinx.coroutines.flow.Flow

interface DetailsDataSource {

    fun getDetails(id: String): Flow<Either<String, CharacterDetails>>
}
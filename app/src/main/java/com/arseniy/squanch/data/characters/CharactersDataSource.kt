package com.arseniy.squanch.data.characters

import com.arseniy.squanch.common.Either
import com.arseniy.squanch.model.Character
import kotlinx.coroutines.flow.Flow

interface CharactersDataSource {

    fun getCharacters(): Flow<Either<String, List<Character>>>
}
package com.arseniy.squanch.common

import com.arseniy.squanch.presentation.characters.CharactersFragment
import com.arseniy.squanch.presentation.details.DetailsFragment
import com.github.terrakok.cicerone.androidx.FragmentScreen

object Screens {

    fun characters() = FragmentScreen { CharactersFragment() }

    fun details(id: String) = FragmentScreen { DetailsFragment.create(id) }
}
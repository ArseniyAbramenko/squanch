package com.arseniy.squanch.common

import com.apollographql.apollo.ApolloClient
import com.arseniy.squanch.data.characters.CharactersDataSource
import com.arseniy.squanch.data.characters.NetworkCharactersDataSource
import com.arseniy.squanch.data.details.DetailsDataSource
import com.arseniy.squanch.data.details.NetworkDetailsDataSource
import com.arseniy.squanch.presentation.characters.*
import com.arseniy.squanch.presentation.details.DetailsViewModel
import com.arseniy.squanch.presentation.details.detailsFeature
import com.arseniy.squanch.presentation.main.MainViewModel
import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.Router
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named
import org.koin.dsl.module

private const val characters = "characters"
private const val details = "details"

val appModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { CharactersViewModel(get(named(characters))) }
    viewModel { (id: String) -> DetailsViewModel(get(named(details)) { parametersOf(id) }) }

    factory(named(characters)) { charactersFeature(get()) }
    factory(named(details)) { (id: String) -> detailsFeature(get(), id) }

    single { charactersEffectHandlers(get(), get()) }
    single<CharactersDataSource> { NetworkCharactersDataSource(get()) }
    single<DetailsDataSource> { NetworkDetailsDataSource(get()) }
}

val navigationModule = module {
    val cicerone = Cicerone.create(Router())
    single { cicerone.router }
    single { cicerone.getNavigatorHolder() }
}

val networkModule = module {
    single {
        ApolloClient.builder()
            .serverUrl("https://rickandmortyapi.com/graphql")
            .build()
    }
}
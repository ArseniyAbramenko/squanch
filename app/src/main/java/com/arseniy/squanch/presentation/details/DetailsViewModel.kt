package com.arseniy.squanch.presentation.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.StateFlow

class DetailsViewModel(feature: DetailsFeature) : ViewModel() {

    val state: StateFlow<DetailsState> = feature.state

    init {
        feature.setup(viewModelScope)
    }
}
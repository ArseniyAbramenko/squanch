package com.arseniy.squanch.presentation.details

import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.arseniy.squanch.R
import com.arseniy.squanch.model.CharacterDetails
import com.arseniy.squanch.model.fakeCharacter
import com.arseniy.squanch.presentation.common.CenteredProgressIndicator
import com.arseniy.squanch.presentation.common.CenteredText
import com.arseniy.squanch.presentation.common.StandardImage
import com.arseniy.squanch.presentation.common.unknownIfNullEmptyOrBlank
import com.arseniy.squanch.theme.SquanchTheme
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.insets.navigationBarsPadding

@Composable
fun DetailsScreen(viewModel: DetailsViewModel) {
    val state: DetailsState by viewModel.state.collectAsState()
    DetailsScreen(state)
}

@Composable
fun DetailsScreen(state: DetailsState) {
    Surface {
        ProvideWindowInsets {
            with(state) {
                if (loading) CenteredProgressIndicator()
                character?.let { CharacterDetails(it) }
                errorMessage?.let { CenteredText(it) }
            }
        }
    }
}

@Composable
fun CharacterDetails(character: CharacterDetails) {
    val scrollState = rememberScrollState()
    ConstraintLayout(
        modifier = Modifier
            .verticalScroll(scrollState)
            .padding(bottom = 16.dp)
            .navigationBarsPadding()
    ) {
        val (image, name, species, status, origin, location) = createRefs()
        val startGuideline = createGuidelineFromStart(16.dp)
        val endGuideline = createGuidelineFromEnd(16.dp)
        val centerGuideline = createGuidelineFromStart(0.5F)

        StandardImage(
            url = character.imageUrl,
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1F)
                .constrainAs(image) {
                    top.linkTo(parent.top)
                }
        )

        Text(
            text = character.name,
            fontSize = 36.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
                .constrainAs(name) {
                    top.linkTo(image.bottom)
                }
        )

        Text(
            text = stringResource(R.string.species, character.species.unknownIfNullEmptyOrBlank()),
            fontSize = 20.sp,
            modifier = Modifier
                .constrainAs(species) {
                    top.linkTo(name.bottom)
                    start.linkTo(startGuideline)
                    end.linkTo(centerGuideline, margin = 16.dp)
                    width = Dimension.fillToConstraints
                }
        )

        Text(
            text = stringResource(R.string.origin, character.origin?.name.unknownIfNullEmptyOrBlank()),
            fontSize = 20.sp,
            modifier = Modifier
                .constrainAs(origin) {
                    top.linkTo(species.bottom, margin = 16.dp)
                    start.linkTo(startGuideline)
                    end.linkTo(centerGuideline, margin = 16.dp)
                    width = Dimension.fillToConstraints
                }
        )

        Text(
            text = stringResource(R.string.status, character.status.unknownIfNullEmptyOrBlank()),
            fontSize = 20.sp,
            modifier = Modifier
                .constrainAs(status) {
                    top.linkTo(name.bottom)
                    start.linkTo(centerGuideline, margin = 16.dp)
                    end.linkTo(endGuideline)
                    width = Dimension.fillToConstraints
                }
        )

        Text(
            text = stringResource(R.string.location, character.location?.name.unknownIfNullEmptyOrBlank()),
            fontSize = 20.sp,
            modifier = Modifier
                .constrainAs(location) {
                    top.linkTo(status.bottom, margin = 16.dp)
                    start.linkTo(centerGuideline, margin = 16.dp)
                    end.linkTo(endGuideline)
                    width = Dimension.fillToConstraints
                }
        )
    }
}

@Preview(showBackground = true)
@Composable
fun LightDetailsPreview() {
    SquanchTheme(darkTheme = false) {
        DetailsScreen(DetailsState(character = fakeCharacter))
    }
}

@Preview(showBackground = true)
@Composable
fun LightLoadingPreview() {
    SquanchTheme(darkTheme = false) {
        DetailsScreen(DetailsState(loading = true))
    }
}

@Preview(showBackground = true)
@Composable
fun LightErrorPreview() {
    SquanchTheme(darkTheme = false) {
        DetailsScreen(DetailsState(errorMessage = "Error message"))
    }
}

@Preview(showBackground = true)
@Composable
fun DarkDetailsPreview() {
    SquanchTheme(darkTheme = true) {
        DetailsScreen(DetailsState(character = fakeCharacter))
    }
}

@Preview(showBackground = true)
@Composable
fun DarkLoadingPreview() {
    SquanchTheme(darkTheme = true) {
        DetailsScreen(DetailsState(loading = true))
    }
}

@Preview(showBackground = true)
@Composable
fun DarkErrorPreview() {
    SquanchTheme(darkTheme = true) {
        DetailsScreen(DetailsState(errorMessage = "Error message"))
    }
}

package com.arseniy.squanch.presentation.characters

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.StateFlow

class CharactersViewModel(private val feature: CharactersFeature) : ViewModel() {

    val state: StateFlow<CharactersState> = feature.state

    init {
        feature.setup(viewModelScope)
    }

    fun selectCharacter(id: String) {
        feature.submitMessage(CharactersMessage.CharacterSelected(id))
    }
}
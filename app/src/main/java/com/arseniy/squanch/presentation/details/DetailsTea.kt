package com.arseniy.squanch.presentation.details

import com.arseniy.squanch.common.*
import com.arseniy.squanch.data.details.DetailsDataSource
import com.arseniy.squanch.model.CharacterDetails
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

data class DetailsState(
    val loading: Boolean = false,
    val character: CharacterDetails? = null,
    val errorMessage: String? = null
) : State

sealed class DetailsMessage : Message {
    object Loading : DetailsMessage()
    data class Success(val character: CharacterDetails) : DetailsMessage()
    data class Error(val errorMessage: String) : DetailsMessage()
}

sealed class DetailsEffect : Effect {
    data class LoadCharacter(val id: String) : DetailsEffect()
}

typealias DetailsFeature = Feature<DetailsState, DetailsMessage, DetailsEffect>

fun detailsFeature(dataSource: DetailsDataSource, id: String) = DetailsFeature(
    initials = FeatureInitials(
        DetailsState(),
        DetailsEffect.LoadCharacter(id)
    ),
    reducer = detailsReducer,
    effectHandlers = setOf(
        loadDetailsEffectHandler(dataSource)
    )
)

val detailsReducer: Reducer<DetailsState, DetailsMessage, DetailsEffect> = { _, message ->
    when (message) {
        is DetailsMessage.Loading -> DetailsState(loading = true) to null
        is DetailsMessage.Success -> DetailsState(character = message.character) to null
        is DetailsMessage.Error -> DetailsState(errorMessage = message.errorMessage) to null
    }
}

@OptIn(ExperimentalCoroutinesApi::class)
fun loadDetailsEffectHandler(dataSource: DetailsDataSource) =
    effectHandler<DetailsEffect.LoadCharacter, DetailsMessage> {
        flatMapLatest { effect ->
            dataSource.getDetails(effect.id)
                .map { it.toMessage() }
                .onStart { emit(DetailsMessage.Loading) }
        }
    }

private fun Either<String, CharacterDetails>.toMessage(): DetailsMessage = when (this) {
    is Either.Left -> DetailsMessage.Error(left)
    is Either.Right -> DetailsMessage.Success(right)
}
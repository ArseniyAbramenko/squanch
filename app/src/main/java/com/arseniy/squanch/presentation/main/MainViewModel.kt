package com.arseniy.squanch.presentation.main

import androidx.lifecycle.ViewModel
import com.arseniy.squanch.common.Screens
import com.github.terrakok.cicerone.Router

class MainViewModel(router: Router) : ViewModel() {

    init {
        router.newRootScreen(Screens.characters())
    }
}
package com.arseniy.squanch.presentation.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import com.arseniy.squanch.delegates.argument
import com.arseniy.squanch.extensions.withStringArgs
import com.arseniy.squanch.presentation.common.DETAILS_ID
import com.arseniy.squanch.theme.SquanchTheme
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class DetailsFragment : Fragment() {

    companion object {
        fun create(id: String) = DetailsFragment().withStringArgs(
            DETAILS_ID to id
        )
    }

    private val id by argument(DETAILS_ID, "")
    private val viewModel: DetailsViewModel by viewModel { parametersOf(id) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View =
        ComposeView(requireContext()).apply {
            setContent {
                SquanchTheme {
                    DetailsScreen(viewModel)
                }
            }
        }
}
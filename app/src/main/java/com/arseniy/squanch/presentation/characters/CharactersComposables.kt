package com.arseniy.squanch.presentation.characters

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.tooling.preview.Preview
import com.arseniy.squanch.theme.SquanchTheme
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.transform.CircleCropTransformation
import com.arseniy.squanch.model.Character
import com.arseniy.squanch.model.fakeCharacters
import com.arseniy.squanch.presentation.common.CenteredProgressIndicator
import com.arseniy.squanch.presentation.common.CenteredText
import com.arseniy.squanch.presentation.common.StandardImage
import com.arseniy.squanch.presentation.common.withSystemBarsInsets
import com.google.accompanist.insets.ProvideWindowInsets

@Composable
fun CharactersScreen(viewModel: CharactersViewModel) {
    val state: CharactersState by viewModel.state.collectAsState()
    CharactersScreen(state, viewModel::selectCharacter)
}

@Composable
fun CharactersScreen(state: CharactersState, onClick: (String) -> Unit) {
    Surface {
        ProvideWindowInsets {
            with(state) {
                if (loading) CenteredProgressIndicator()
                characters?.let { CharactersList(it, onClick) }
                errorMessage?.let { CenteredText(it) }
            }
        }
    }
}

@Composable
fun CharactersList(characters: List<Character>, onClick: (String) -> Unit) {
    LazyColumn(
        contentPadding = PaddingValues(all = 16.dp).withSystemBarsInsets()
    ) {
        items(characters) { character ->
            CharacterListItem(character, onClick)
        }
    }
}

@Composable
fun CharacterListItem(character: Character, onClick: (String) -> Unit) {
    Card(
        elevation = 8.dp,
        modifier = Modifier
            .padding(16.dp)
            .clickable { onClick(character.id) }
            .fillMaxWidth()
    ) {
        Row(modifier = Modifier.padding(16.dp)) {
            StandardImage(
                url = character.imageUrl,
                builder = { transformations(CircleCropTransformation()) },
                modifier = Modifier
                    .size(96.dp)
                    .align(Alignment.CenterVertically)
            )

            Column(
                modifier = Modifier.padding(start = 8.dp)
            ) {
                CharacterText(character.name)
                CharacterText(character.status)
                CharacterText(character.species)
            }
        }
    }
}

@Composable
fun CharacterText(text: String) {
    Text(
        text = text,
        modifier = Modifier.padding(8.dp)
    )
}

@Preview(showBackground = true)
@Composable
fun LightListPreview() {
    SquanchTheme(darkTheme = false) {
        CharactersScreen(CharactersState(characters = fakeCharacters)) {}
    }
}

@Preview(showBackground = true)
@Composable
fun LightLoadingPreview() {
    SquanchTheme(darkTheme = false) {
        CharactersScreen(CharactersState(loading = true)) {}
    }
}

@Preview(showBackground = true)
@Composable
fun LightErrorPreview() {
    SquanchTheme(darkTheme = false) {
        CharactersScreen(CharactersState(errorMessage = "Error message")) {}
    }
}

@Preview(showBackground = true)
@Composable
fun DarkListPreview() {
    SquanchTheme(darkTheme = true) {
        CharactersScreen(CharactersState(characters = fakeCharacters)) {}
    }
}

@Preview(showBackground = true)
@Composable
fun DarkLoadingPreview() {
    SquanchTheme(darkTheme = true) {
        CharactersScreen(CharactersState(loading = true)) {}
    }
}

@Preview(showBackground = true)
@Composable
fun DarkErrorPreview() {
    SquanchTheme(darkTheme = true) {
        CharactersScreen(CharactersState(errorMessage = "Error message")) {}
    }
}
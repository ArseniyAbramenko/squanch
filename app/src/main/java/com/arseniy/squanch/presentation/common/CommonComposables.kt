package com.arseniy.squanch.presentation.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.stringResource
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import coil.request.ImageRequest
import com.arseniy.squanch.R
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.rememberInsetsPaddingValues

@OptIn(ExperimentalCoilApi::class)
@Composable
fun StandardImage(
    url: String,
    modifier: Modifier,
    builder: ImageRequest.Builder.() -> Unit = {}
) {
    Image(
        painter = rememberImagePainter(
            data = url,
            builder = builder
        ),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = modifier
    )
}

@Composable
fun CenteredProgressIndicator() {
    Box(Modifier.fillMaxSize()) {
        CircularProgressIndicator(
            color = MaterialTheme.colors.onBackground,
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
fun CenteredText(text: String) {
    Box(Modifier.fillMaxSize()) {
        Text(
            text = text,
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
fun String?.unknownIfNullEmptyOrBlank(): String =
    if (this != null && isNotEmpty() && isNotBlank()) this
    else stringResource(R.string.unknown)

@Composable
fun PaddingValues.withSystemBarsInsets(): PaddingValues =
    this + rememberInsetsPaddingValues(LocalWindowInsets.current.systemBars)

@Composable
private operator fun PaddingValues.plus(another: PaddingValues): PaddingValues = PaddingValues(
    start = another.calculateStartPadding(LocalLayoutDirection.current),
    top = another.calculateTopPadding(),
    end = another.calculateEndPadding(LocalLayoutDirection.current),
    bottom = another.calculateBottomPadding()
)
package com.arseniy.squanch.presentation.characters

import com.arseniy.squanch.common.*
import com.arseniy.squanch.data.characters.CharactersDataSource
import com.arseniy.squanch.model.Character
import com.github.terrakok.cicerone.Router
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

data class CharactersState(
    val loading: Boolean = false,
    val characters: List<Character>? = null,
    val errorMessage: String? = null
) : State

sealed class CharactersMessage : Message {
    object Loading : CharactersMessage()
    object None : CharactersMessage()
    data class Success(val characters: List<Character>) : CharactersMessage()
    data class Error(val errorMessage: String) : CharactersMessage()
    data class CharacterSelected(val id: String) : CharactersMessage()
}

sealed class CharactersEffect : Effect {
    object LoadCharacters : CharactersEffect()
    data class NavigateToDetails(val id: String) : CharactersEffect()
}

typealias CharactersFeature = Feature<CharactersState, CharactersMessage, CharactersEffect>

fun charactersFeature(effectHandlers: Set<EffectHandler<CharactersEffect, CharactersMessage>>) = CharactersFeature(
    initials = FeatureInitials(
        CharactersState(),
        CharactersEffect.LoadCharacters
    ),
    reducer = charactersReducer,
    effectHandlers = effectHandlers
)

fun charactersEffectHandlers(
    dataSource: CharactersDataSource,
    router: Router
): Set<EffectHandler<CharactersEffect, CharactersMessage>> = setOf(
    loadCharactersEffectHandler(dataSource),
    navigateToDetailsEffectHandler(router)
)

val charactersReducer: Reducer<CharactersState, CharactersMessage, CharactersEffect> = { state, message ->
    when (message) {
        is CharactersMessage.Loading -> CharactersState(loading = true) to null
        is CharactersMessage.Success -> CharactersState(characters = message.characters) to null
        is CharactersMessage.Error -> CharactersState(errorMessage = message.errorMessage) to null
        is CharactersMessage.CharacterSelected -> state to CharactersEffect.NavigateToDetails(message.id)
        is CharactersMessage.None -> state to null
    }
}

@OptIn(ExperimentalCoroutinesApi::class)
fun loadCharactersEffectHandler(dataSource: CharactersDataSource) =
    effectHandler<CharactersEffect.LoadCharacters, CharactersMessage> {
        flatMapLatest { _ ->
            dataSource.getCharacters()
                .map { it.toMessage() }
                .onStart { emit(CharactersMessage.Loading) }
        }
    }

fun navigateToDetailsEffectHandler(router: Router) =
    effectHandler<CharactersEffect.NavigateToDetails, CharactersMessage> {
        map { effect ->
            router.navigateTo(Screens.details(effect.id))
            CharactersMessage.None
        }
    }

private fun Either<String, List<Character>>.toMessage(): CharactersMessage = when (this) {
    is Either.Left -> CharactersMessage.Error(left)
    is Either.Right -> CharactersMessage.Success(right)
}
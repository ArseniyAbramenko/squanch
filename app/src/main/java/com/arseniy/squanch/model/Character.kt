package com.arseniy.squanch.model

data class Character(
    val id: String,
    val name: String,
    val status: String,
    val species: String,
    val imageUrl: String
)

val fakeCharacters = listOf(
    Character(
        id = "1",
        name = "Rick Sanchez",
        status = "Alive",
        species = "Human",
        imageUrl = "https://rickandmortyapi.com/api/character/avatar/1.jpeg"
    ),
    Character(
        id = "2",
        name = "Morty Smith",
        status = "Alive",
        species = "Human",
        imageUrl = "https://rickandmortyapi.com/api/character/avatar/2.jpeg"
    ),
)

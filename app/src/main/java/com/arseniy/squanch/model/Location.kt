package com.arseniy.squanch.model

data class Location(
    val id: String,
    val name: String
)

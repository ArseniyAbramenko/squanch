package com.arseniy.squanch.model

data class CharacterDetails(
    val id: String,
    val name: String,
    val status: String,
    val species: String,
    val origin: Location?,
    val location: Location?,
    val imageUrl: String
)

val fakeCharacter = CharacterDetails(
    id = "1",
    name = "Rick Sanchez",
    status = "Alive",
    species = "Human",
    origin = Location(id = "1", name = "Earth (C-137)"),
    location = Location(id = "20", name = "Earth (Replacement Dimension)"),
    imageUrl = "https://rickandmortyapi.com/api/character/avatar/1.jpeg"
)

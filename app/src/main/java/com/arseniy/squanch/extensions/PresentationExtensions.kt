package com.arseniy.squanch.extensions

import android.os.Bundle
import androidx.fragment.app.Fragment

fun <T : Fragment> T.withStringArgs(vararg args: Pair<String, String>): T = apply {
    arguments = Bundle().apply {
        args.forEach { (key, value) ->
            putString(key, value)
        }
    }
}
package com.arseniy.squanch.extensions

import com.apollographql.apollo.api.Response
import com.arseniy.squanch.common.Either

fun <T, D> Response<T>.toEither(
    dataMapper: Response<T>.() -> D?
): Either<String, D> =
    toEither(defaultErrorMapper, dataMapper)

fun <T, D, E> Response<T>.toEither(
    errorMapper: Response<T>.() -> E,
    dataMapper: Response<T>.() -> D?
): Either<E, D> =
    dataMapper()
        ?.let { Either.Right(it) }
        ?: Either.Left(errorMapper())

private typealias DefaultErrorMapper = Response<*>.() -> String

private val defaultErrorMapper: DefaultErrorMapper = {
    errors
        ?.joinToString(separator = "\n") { it.message }
        .let { it ?: "" }
}

package com.arseniy.squanch.details

import com.arseniy.squanch.common.FeatureInitials
import com.arseniy.squanch.data.details.DetailsDataSource
import com.arseniy.squanch.data.details.ErrorFakeDetailsDataSource
import com.arseniy.squanch.data.details.FAKE_DETAILS_ERROR_MESSAGE
import com.arseniy.squanch.data.details.SuccessFakeDetailsDataSource
import com.arseniy.squanch.model.fakeCharacter
import com.arseniy.squanch.presentation.details.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Test
import kotlin.time.ExperimentalTime

class DetailsTeaTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    private val testScope = TestCoroutineScope(TestCoroutineDispatcher())

    @ExperimentalTime
    @Test
    fun `details feature success test`() = testFeature(
        dataSource = SuccessFakeDetailsDataSource(),
        expectedStates = expectedStates(
            finalState = DetailsState(
                character = fakeCharacter
            )
        )
    )

    @ExperimentalTime
    @Test
    fun `characters feature error test`() = testFeature(
        dataSource = ErrorFakeDetailsDataSource(),
        expectedStates = expectedStates(
            finalState = DetailsState(
                errorMessage = FAKE_DETAILS_ERROR_MESSAGE
            )
        )
    )

    @OptIn(ExperimentalCoroutinesApi::class)
    @ExperimentalTime
    @Test
    fun `characters reducer test`() = testScope.runBlockingTest {
        val initialState = DetailsState()
        assertEquals(
            DetailsState(loading = true) to null,
            detailsReducer(initialState, DetailsMessage.Loading)
        )
        assertEquals(
            DetailsState(character = fakeCharacter) to null,
            detailsReducer(initialState, DetailsMessage.Success(fakeCharacter))
        )
        assertEquals(
            DetailsState(errorMessage = FAKE_DETAILS_ERROR_MESSAGE) to null,
            detailsReducer(initialState, DetailsMessage.Error(FAKE_DETAILS_ERROR_MESSAGE))
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun testFeature(
        dataSource: DetailsDataSource,
        expectedStates: List<DetailsState>
    ) =
        testScope.runBlockingTest {
            val result = mutableListOf<DetailsState>()

            DetailsFeature(
                initials = FeatureInitials(
                    DetailsState(),
                    DetailsEffect.LoadCharacter(fakeCharacter.id)
                ),
                reducer = detailsReducer,
                effectHandlers = setOf(
                    loadDetailsEffectHandler(dataSource)
                )
            ).run {
                testScope.launch {
                    state.toList(result)
                }
                setup(testScope)
            }

            assertEquals(expectedStates, result)
        }

    private fun expectedStates(finalState: DetailsState) = listOf(
        DetailsState(),
        DetailsState(loading = true),
        finalState
    )
}
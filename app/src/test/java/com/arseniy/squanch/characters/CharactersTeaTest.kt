package com.arseniy.squanch.characters

import com.arseniy.squanch.common.FeatureInitials
import com.arseniy.squanch.data.characters.CharactersDataSource
import com.arseniy.squanch.data.characters.ErrorFakeCharactersDataSource
import com.arseniy.squanch.data.characters.FAKE_CHARACTERS_ERROR_MESSAGE
import com.arseniy.squanch.data.characters.SuccessFakeCharactersDataSource
import com.arseniy.squanch.model.fakeCharacter
import com.arseniy.squanch.model.fakeCharacters
import com.arseniy.squanch.presentation.characters.*
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.junit.Assert.*
import kotlin.time.ExperimentalTime

class CharactersTeaTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    private val testScope = TestCoroutineScope(TestCoroutineDispatcher())

    @ExperimentalTime
    @Test
    fun `characters feature success test`() = testFeature(
        dataSource = SuccessFakeCharactersDataSource(),
        expectedStates = expectedStates(
            finalState = CharactersState(
                characters = fakeCharacters
            )
        )
    )

    @ExperimentalTime
    @Test
    fun `characters feature error test`() = testFeature(
        dataSource = ErrorFakeCharactersDataSource(),
        expectedStates = expectedStates(
            finalState = CharactersState(
                errorMessage = FAKE_CHARACTERS_ERROR_MESSAGE
            )
        )
    )

    @OptIn(ExperimentalCoroutinesApi::class)
    @ExperimentalTime
    @Test
    fun `characters reducer test`() = testScope.runBlockingTest {
        val initialState = CharactersState()
        assertEquals(
            CharactersState(loading = true) to null,
            charactersReducer(initialState, CharactersMessage.Loading)
        )
        assertEquals(
            CharactersState(characters = fakeCharacters) to null,
            charactersReducer(initialState, CharactersMessage.Success(fakeCharacters))
        )
        assertEquals(
            CharactersState(errorMessage = FAKE_CHARACTERS_ERROR_MESSAGE) to null,
            charactersReducer(initialState, CharactersMessage.Error(FAKE_CHARACTERS_ERROR_MESSAGE))
        )
        assertEquals(
            initialState to CharactersEffect.NavigateToDetails(fakeCharacter.id),
            charactersReducer(initialState, CharactersMessage.CharacterSelected(fakeCharacter.id))
        )
        assertEquals(
            initialState to null,
            charactersReducer(initialState, CharactersMessage.None)
        )
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun testFeature(
        dataSource: CharactersDataSource,
        expectedStates: List<CharactersState>
    ) =
        testScope.runBlockingTest {
            val result = mutableListOf<CharactersState>()

            CharactersFeature(
                initials = FeatureInitials(
                    CharactersState(),
                    CharactersEffect.LoadCharacters
                ),
                reducer = charactersReducer,
                effectHandlers = setOf(
                    loadCharactersEffectHandler(dataSource),
                    navigateToDetailsEffectHandler(mockk(relaxUnitFun = true))
                )
            ).run {
                testScope.launch {
                    state.toList(result)
                }
                setup(testScope)
            }

            assertEquals(expectedStates, result)
        }

    private fun expectedStates(finalState: CharactersState) = listOf(
        CharactersState(),
        CharactersState(loading = true),
        finalState
    )
}
